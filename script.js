function createList(arr, parent = document.body) {
    const ulList = document.createElement('ul');
    arr.forEach((item) => {
        const liList = document.createElement('li')
        if (Array.isArray(item)){
            createList(item, liList)
        } else { liList.textContent = item }
        ulList.appendChild(liList)
    });
    parent.appendChild(ulList);
}

function clearPage() {
    document.body.innerHTML = ' ';
}

const array = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
const array2 = ["1", "2", "3", "sea", "user", 23];
createList(array);
createList(array2);

setTimeout(clearPage, 3000)

